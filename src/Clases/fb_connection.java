package Clases;

import Formulario.frmModificarPadre;
import Formulario.frmPagos;
import Formulario.frmRegistroPadre;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class fb_connection {

    private Connection cnn;
    private ResultSet resultSet;
    private Statement statement;

    public Connection getCnn() {
        return cnn;
    }

    public void setCnn(Connection cnn) {
        this.cnn = cnn;
    }

    public fb_connection() {
        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");
            Properties props = new Properties();
            props.setProperty("user", "SYSDBA");
            //props.setProperty("password", "Yironsel321");
            props.setProperty("password", "123uasd");
            cnn = DriverManager.getConnection("jdbc:firebirdsql://127.0.0.1/"
                    + "SophiaStudio", props);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cerrarConexion() {
        try {
            cnn.close();
            resultSet.close();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean validarUsuario(String usuario, String clave) {
        try {
            String sql = "select (1) from usuarios where nombres = '"
                    + usuario + "' and clave = '" + clave + "' and estado = 1";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            return resultSet.next();
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean existeUsuario(String usuario) {
        try {
            String sql = "select (1) from usuarios where idUsuario = '"
                    + usuario + "'";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            return resultSet.next();
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean existeEstudiante(String buscar) {
        try {
            String sql = "select (1) from estudiantes where matricula like '"
                    + buscar + "' or nombres like '" + buscar + "' or apellidos like '"
                    + buscar + "'";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            return resultSet.next();
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean estadoEstudiante(String matricula) {
        try {
            String sql = "select (1) from estudiantes e where e.matricula = " + matricula + " and e.estado = 1";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            return resultSet.next();
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean validarPadreMadre(String miPadreMadre) {
        try {
            String sql = "select 1 from padremadres where documento like '" + miPadreMadre + "'";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            return resultSet.next();
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public Integer getPerfil(String usuario) {
        try {
            String sql = "select idPerfil from usuarios where nombres LIKE '"
                    + usuario + "'";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getInt("idPerfil");
            } else {
                return -1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public String getPatch(String patch) {
        try {
            String sql = "select patch from UBICACION_REPORTES where Descripcion LIKE '" + patch + "'";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getString("Patch");
            } else {
                return "C:\\";
            }
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "C:\\";
        }
    }

    public Integer getMaxMatricula() {
        try {
            String sql = "select max(e.MATRICULA) from estudiantes e";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return -1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public Integer valorMax() {
        try {
            String sql = "select max(idusuario) from usuarios";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Integer valorMaxTanda() {
        try {
            String sql = "select count(ID_TANDA) from tandas";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Integer valorMaxPerfiles() {
        try {
            String sql = "select count(idperfil) from perfiles";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Integer cantidadPatch() {
        try {
            String sql = "select count(id) from UBICACION_REPORTES";
            statement = cnn.createStatement();
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public String agregarEstudiante(Estudiantes estudiante) {
        try {
            String sql = "INSERT INTO estudiantes(Cedula_PadreMadre, NOMBRES, APELLIDOS, ID_TANDA,"
                    + " FECHANACIMIENTO, ESTADO) VALUES('"
                    + estudiante.getPadreMadreCedula() + "', '"
                    + estudiante.getNombres() + "', '"
                    + estudiante.getApellidos() + "', "
                    + estudiante.getTanda() + ", '"
                    + estudiante.getFechaNacimiento() + "', '"
                    + estudiante.getEstado() + "')";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Estudiante Agregado Correctamente";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Existe Problemas al agregar Estudiante, Contactar SoftSureña :( ...! \n" + ex.toString();
        }
    }

    public String agregarTanda(Tandas miTanda) {
        try {
            String sql = "insert into TANDAS (ANNO_INICIAL, ANNO_FINAL, "
                    + "HORA_INICIO, HORA_FINAL,LUNES, MARTES, MIERCOLES, JUEVES,"
                    + "VIERNES, SABADOS, DOMINGOS, CANTIDAD_ESTUDIANTES, "
                    + "EDAD_MINIMA, EDAD_MAXIMA, CON_EDAD,ESTADO) "
                    + "values ('" + miTanda.getAnno_inicial() + "', '" + miTanda.getAnno_final() + "', "
                    + "'" + miTanda.getHora_inicial() + "', '" + miTanda.getHora_final() + "', "
                    + "" + miTanda.getLunes() + ", " + miTanda.getMartes() + ", "
                    + "" + miTanda.getMiercoles() + ", " + miTanda.getJueves() + ", "
                    + "" + miTanda.getViernes() + ", " + miTanda.getSabados() + ", "
                    + "" + miTanda.getDomingos() + ", " + miTanda.getCantidad_estudiantes() + ", "
                    + "" + miTanda.getEdad_minima() + ", " + miTanda.getEdad_maxima() + ", " + miTanda.getCon_edad() + ", 0)";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Tanda Agregada Correctamente";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Existe Problemas al agregar Tanda, Contactar SoftSureña :( ...! \n" + ex.toString();
        }
    }

    public String insertarEstudiante(Estudiantes miEstudiante) {
        try {
            String sql = "EXECUTE PROCEDURE CREAR_ESTUDIANTE('"
                    + miEstudiante.getPadreMadreCedula() + "','"
                    + miEstudiante.getNombres() + "','"
                    + miEstudiante.getApellidos() + "',"
                    + miEstudiante.getTanda() + ",'"
                    + miEstudiante.getFechaNacimiento() + "', "
                    + miEstudiante.getIdUsuario() + ")";
            statement = cnn.createStatement();
            statement.execute(sql);
            return "Estudiante Agregado Correctamente";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "No Agregado el Estudiante";
        }
    }

    public void pPagoMensualidad(String idUsuario, String pago,
            String matricula, String fechaPago) {
        frmPagos miPago = new frmPagos();
        try {
            String sql = "EXECUTE PROCEDURE P_PAGO_MENSUALIDAD(" + idUsuario + ","
                    + pago + "," + matricula + ",'" + fechaPago + "')";
            statement = cnn.createStatement();
            statement.execute(sql);
            JOptionPane.showMessageDialog(miPago, "Pago Realizado");
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);

            JOptionPane.showMessageDialog(miPago, ex.getMessage());
        }
    }

    public String inscribirEstudiante(Estudiantes miEstudiante, String pago) {
        try {
            String sql = "EXECUTE PROCEDURE P_INSCRIPCION ("
                    + miEstudiante.getMatricula() + ", '"
                    + miEstudiante.getPadreMadreCedula() + "', "
                    + miEstudiante.getTanda() + ", "
                    + pago + " ," + miEstudiante.getIdUsuario() + ",1)";
            statement = cnn.createStatement();
            statement.execute(sql);
            return "Alumno Inscripto...";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return ex.getMessage();
        }
    }

    public String agregarUsuario(Usuarios miUsuario) {
        
        try {
            String sql = "insert into usuarios (nombres,"
                    + " apellidos, clave, idperfil, estado) values('"
                    + miUsuario.getNombres() + "', '"
                    + miUsuario.getApellidos() + "', '"
                    + miUsuario.getClave() + "', "
                    + miUsuario.getPerfil() + ", "
                    + miUsuario.getEstado() + ")";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Usuario Agregado Exitosamente!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Usuario NO Agregado :( \n" + ex.toString();
        }
    }

    public String agregarPadreMadre(PadreMadres miPadre) {
        try {
            String sql = "insert into PADREMADRES(documento, nombres, apellidos, "
                    + "sexo, telefono, telefono2, direccion, estado, correo, idUsuario) "
                    + "values('" + miPadre.getCedula() + "', "
                    + "'" + miPadre.getNombres() + "', "
                    + "'" + miPadre.getApellidos() + "', "
                    + "'" + miPadre.getSexo() + "', "
                    + "'" + miPadre.getTelefono() + "', "
                    + "'" + miPadre.getTelefono2() + "', "
                    + "'" + miPadre.getDireccion() + "', "
                    + "'" + miPadre.getEstado() + "', "
                    + "'" + miPadre.getCorreo() + "', " + miPadre.getIdUsuario() + ")";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Padre Agregado Exitosamente...!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            frmRegistroPadre miReg = new frmRegistroPadre();
            JOptionPane.showMessageDialog(miReg, ex.getMessage());
            return "Padre no Agregado :( ...!";
        }
    }

    public String agregarPerfil(String Perfil) {
        try {
            String sql = "insert into Perfiles(perfil) values('" + Perfil + "')";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Perfil Creado";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Perfil no Creado:( ...! \n" + ex.toString();
        }
    }

    public String modificarPadreMadre(PadreMadres miPadre) {
        try {
            String sql = "update PADREMADRES set "
                    + "Documento = '" + miPadre.getCedula() + "', "
                    + "nombres = '" + miPadre.getNombres() + "', "
                    + "apellidos = '" + miPadre.getApellidos() + "', "
                    + "sexo = '" + miPadre.getSexo() + "', "
                    + "telefono = '" + miPadre.getTelefono() + "', "
                    + "telefono2 = '" + miPadre.getTelefono2() + "', "
                    + "direccion = '" + miPadre.getDireccion() + "', "
                    + "estado = '" + miPadre.getEstado() + "', "
                    + "correo = '" + miPadre.getCorreo() + "', "
                    + "idUsuario = " + miPadre.getIdUsuario() + " "
                    + " where id_PadreMadres = " + miPadre.getIdPadreMadre() + "";
            statement = cnn.createStatement();
            statement.execute(sql);
            frmModificarPadre miMOdPad = new frmModificarPadre();
            JOptionPane.showMessageDialog(miMOdPad, "Padre Modificado Exitosamente...!");
            return "Padre Modificado Exitosamente...!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            frmModificarPadre miMOdPad = new frmModificarPadre();
            JOptionPane.showMessageDialog(miMOdPad, ex.getMessage());
            return "Padre NO Modificado :( ...!";
        }
    }

    public String modificarPerfil(Perfiles perfiles) {
        try {
            String sql = "update Perfiles set "
                    + "perfil = '" + perfiles.getPerfil() + "', "
                    + "frm_Padres = " + perfiles.getFrm_Padres() + ", "
                    + "frm_Padres_Registro = " + perfiles.getFrm_Padres_Registro() + ", "
                    + "frm_Padres_Modificar = " + perfiles.getFrm_Padres_Modificar() + ", "
                    + "frm_Padres_Borrar = " + perfiles.getFrm_Padres_Borrar() + ", "
                    + "frm_Estudiantes = " + perfiles.getFrm_Estudiantes() + ", "
                    + "frm_Estudiantes_Registro = " + perfiles.getFrm_Estudiantes_Registro() + ", "
                    + "frm_Estudiantes_Modificar = " + perfiles.getFrm_Estudiantes_Modificar() + ", "
                    + "frm_Estudiantes_Borrar = " + perfiles.getFrm_Estudiantes_Borrar() + ", "
                    + "frm_Usuarios = " + perfiles.getFrm_Usuarios() + ", "
                    + "frm_Usuarios_registro = " + perfiles.getFrm_Usuarios_Registro() + ", "
                    + "frm_Usuarios_Modificar = " + perfiles.getFrm_Usuarios_Modificar() + ", "
                    + "frm_Usuarios_Borrar = " + perfiles.getFrm_Usuarios_Borrar() + ", "
                    + "frm_Pagos = " + perfiles.getFrm_Pagos() + ", "
                    + "frm_Abono = " + perfiles.getFrm_Abono() + ", "
                    + "frm_reportes = " + perfiles.getFrm_Reportes() + ", "
                    + "frm_ajuste_tandas = " + perfiles.getFrm_Ajuste_Tanda() + ", "
                    + "frm_Ajuste_Perfil_Usuarios = " + perfiles.getFrm_Ajuste_Perfil_Usuario() + " "
                    + "where IdPerfil = " + perfiles.getIdPerfil() + "";
            statement = cnn.createStatement();
            statement.execute(sql);
            return "Perfil Modificado Correctamente Nombre: " + perfiles.getPerfil();
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Perfil no puedo ser Modificado, Nombre: " + perfiles.getPerfil();
        }
    }

    public String modificarUsuario(Usuarios miUsuario) {
        try {
            String sql = "update usuarios set "
                    + "nombres = '" + miUsuario.getNombres() + "', "
                    + "apellidos = '" + miUsuario.getApellidos() + "', "
                    + "clave = '" + miUsuario.getClave() + "', "
                    + "idPerfil = " + miUsuario.getPerfil() + ", "
                    + "estado = " + miUsuario.getEstado() + " "
                    + "where idUsuario = " + miUsuario.getIdUsuario() + "";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Usuario Modificado Exitosamente!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Usuario NO Modificado :( \n" + ex.toString();
        }
    }

    public String modificarUbicaciones(String[] ubicacion) {
        try {
            String sql = "update UBICACION_REPORTES set "
                    + "patch = '" + ubicacion[0] + "' where DESCRIPCION LIKE 'ReporteAlumnos'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);

            sql = "update UBICACION_REPORTES set "
                    + "patch = '" + ubicacion[1] + "' where DESCRIPCION LIKE 'ReporteAlumnosAgrupados'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);

            sql = "update UBICACION_REPORTES set "
                    + "patch = '" + ubicacion[4] + "' where DESCRIPCION LIKE 'Recibo'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);

            sql = "update UBICACION_REPORTES set "
                    + "patch = '" + ubicacion[5] + "' where DESCRIPCION LIKE 'Deuda'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);

            //----------------------------------------------------------------------
            sql = "update UBICACION_REPORTES set "
                    + "Dinero = " + ubicacion[2] + " where DESCRIPCION LIKE 'Mensualidad'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);

            sql = "update UBICACION_REPORTES set "
                    + "Dinero = " + ubicacion[3] + " where DESCRIPCION LIKE 'PagoInscripcion'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);

            return "Ubicaciones Actualizada";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "No se Realizaron las Actualizaciones";
        }
    }

    public String modificarEstudiante(Estudiantes estudiante) {
        try {
            String sql = "update estudiantes set "
                    + "nombres = '" + estudiante.getNombres() + "', "
                    + "apellidos = '" + estudiante.getApellidos() + "', "
                    + "id_tanda = " + estudiante.getTanda() + ", "
                    + "fechaNacimiento = '" + estudiante.getFechaNacimiento() + "', "
                    + "estado = " + estudiante.getEstado() + ", "
                    + "IDUSUARIO = " + estudiante.getIdUsuario() + " "
                    + "where matricula = " + estudiante.getMatricula() + "";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Estudiante Modificado Correctamente...!!!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Estudiante no pudo ser Modificado, Conctate SoftSureña...!!!";
        }
    }

    public String modificarTanda(Tandas miTanda) {
        try {
            String sql = "update TANDAS set "
                    + "ANNO_INICIAL = '" + miTanda.getAnno_inicial() + "', "
                    + "ANNO_FINAL = '" + miTanda.getAnno_final() + "', "
                    + "HORA_INICIO = '" + miTanda.getHora_inicial() + "', "
                    + "HORA_FINAL = '" + miTanda.getHora_final() + "', "
                    + "LUNES = " + miTanda.getLunes() + ", "
                    + "MARTES = " + miTanda.getMartes() + ", "
                    + "MIERCOLES = " + miTanda.getMiercoles() + ", "
                    + "JUEVES = " + miTanda.getJueves() + ", "
                    + "VIERNES = " + miTanda.getViernes() + ", "
                    + "SABADOS = " + miTanda.getSabados() + ", "
                    + "DOMINGOS = " + miTanda.getDomingos() + ", "
                    + "CANTIDAD_ESTUDIANTES = " + miTanda.getCantidad_estudiantes() + ", "
                    + "EDAD_MINIMA = " + miTanda.getEdad_minima() + ", "
                    + "EDAD_MAXIMA = " + miTanda.getEdad_maxima() + ", "
                    + "CON_EDAD = " + miTanda.getCon_edad() + ", "
                    + "ESTADO = 0 "
                    + "where (ID_TANDA = " + miTanda.getId_tanda() + ") ";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Tanda Modificado Correctamente...!!!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Tanda no pudo ser Modificado, Conctate SoftSureña...!!!" + ex.getMessage();
        }
    }

    public String borrarUsuario(String usuario) {
        try {
            String sql = "delete from usuarios where nombres LIKE '" + usuario + "'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Usuario Borrado Correctamente...!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Usuario no pudo ser eliminado...!\n" + ex.toString();
        }
    }

    public String borrarPadreMadres(String Documento) {
        try {
            String sql = "delete from padreMadres where documento LIKE '" + Documento + "'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Padre Borrado Correctamente...!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Padre no pudo ser eliminado...!\n" + ex.toString();
        }
    }

    public String borrarPerfil(String Perfil) {
        try {
            String sql = "delete from Perfiles where perfil like '" + Perfil + "'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Perfil Borrado";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Perfil no pudo ser Borrado";
        }
    }

    public String borrarEstudiante(String usuario) {
        try {
            String sql = "delete from Estudiantes where cedula_PadreMadre LIKE '" + usuario + "'";
            statement = cnn.createStatement();
            statement.executeUpdate(sql);
            return "Estudiantes Borrado Correctamente...!";
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return "Estudiantes no pudo ser eliminado...!\n" + ex.toString();
        }
    }

    public ResultSet getPadreMadres() {
        try {
            String sql = "select * from PADREMADRES";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getPadreMadres(String Padre) {
        try {
            String sql = "select * from PADREMADRES where documento LIKE '" + Padre + "'";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getHorario() {
        try {
            String sql = "select * from TANDAS order by 1";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getMensualidad(String matricula, String periodo) {
        try {
            String sql = "select "
                    + "m.consecutivo, m.fecha_pago, "
                    + "m.Estado, m.monto, m.pagado, m.total,"
                    + " m.fecha_pagado, m.fecha_abono, m.periodo "
                    + "from Mensualidad m where matricula = " + matricula + " and"
                    + " PERIODO like '" + periodo + "'";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getTandas(String edad) {
        try {
            String sql = "select t.id_tanda, trim(case lunes "
                    + "when 1 then 'Lunes '"
                    + "else trim('')"
                    + "end||"
                    + "case martes "
                    + "when 1 then 'Martes '"
                    + "else trim('')"
                    + "end||"
                    + "case miercoles "
                    + "when 1 then 'Miercoles '"
                    + "else trim('')"
                    + "end||"
                    + "case jueves "
                    + "when 1 then 'Jueves '"
                    + "else trim('')"
                    + "end||"
                    + "case viernes "
                    + "when 1 then 'Viernes '"
                    + "else trim('')"
                    + "end||"
                    + "case sabados "
                    + "when 1 then 'Sabados '"
                    + "else trim('')"
                    + "end||"
                    + "case domingos "
                    + "when 1 then 'Domingos '"
                    + "else trim('')"
                    + "end)"
                    + "||' De '||subString(t.Hora_Inicio from 1 for 8)"
                    + "||' Hasta '||subString(t.Hora_Final from 1 for 8)"
                    + "from Tandas t where t.edad_minima <= " + edad + " and t.edad_maxima >= " + edad + "";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getUsuarios(String usuarios) {
        try {
            String sql = "select * from usuarios where nombres LIKE '" + usuarios + "'";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getTandas(Integer id_Tanda) {
        try {
            String sql = "select t.cantidad_estudiantes, t.edad_minima, "
                    + "t.edad_maxima, t.con_edad "
                    + "from Tandas t "
                    + "where t.id_tanda =" + id_Tanda + " ";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getPerfiles(Integer perfil) {
        try {
            String sql = "select * from perfiles where idperfil = " + perfil + " ";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getPerfiles() {
        try {
            String sql = "select * from Perfiles order by 1";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getUbicaciones() {
        try {
            String sql = "select * from UBICACION_REPORTES";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getPerfiles(String perfil) {
        try {
            String sql = "select * from Perfiles WHERE perfil LIKE '" + perfil + "'";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getEstudiante(String matricula) {
        try {
            String sql = "SELECT e.MATRICULA,"
                    + " e.CEDULA_PADREMADRE,"
                    + " e.NOMBRES,"
                    + " e.APELLIDOS,"
                    + " e.FECHANACIMIENTO,"
                    + " e.FECHAINGRESO,"
                    + " e.ESTADO,"
                    + " e.PERIODO_ACTUAL,"
                    + " (select trim("
                    + "case lunes"
                    + " when 1 then 'Lunes '"
                    + " else trim('') "
                    + "end|| "
                    + "case martes"
                    + " when 1 then 'Martes '"
                    + " else trim('') "
                    + "end|| "
                    + "case miercoles"
                    + " when 1 then 'Miercoles '"
                    + " else trim('') "
                    + "end|| "
                    + "case jueves"
                    + " when 1 then 'Jueves '"
                    + " else trim('') "
                    + "end|| "
                    + "case viernes"
                    + " when 1 then 'Viernes '"
                    + " else trim('') "
                    + "end|| "
                    + "case sabados"
                    + " when 1 then 'Sabados '"
                    + " else trim('') "
                    + "end|| "
                    + "case domingos"
                    + " when 1 then 'Domingos '"
                    + " else trim('') "
                    + " end)||' De '|| substring(HORA_INICIO from 1 for 8) ||' '||' Hasta '||substring(HORA_FINAL from 1 for 8)"
                    + " from Tandas t"
                    + " where t.ID_Tanda like e.ID_tanda)as dias, "
                    + " (select p.NOMBRES||' '||p.APELLIDOS "
                    + "from PADREMADRES p"
                    + " where p.DOCUMENTO like e.CEDULA_PADREMADRE) as NombrePadre, "
                    + "ID_tanda "
                    + "FROM estudiantes e "
                    + "where e.MATRICULA = '" + matricula + "'";
            statement = cnn.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(fb_connection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
