package Clases;
import java.util.Date;
/**
 *
 * @author jhiro
 */
public class Tandas {
    private Integer id_tanda;
    private Date anno_inicial;
    private Date anno_final;
    private Date hora_inicial;
    private Date hora_final;
    private int lunes;
    private int martes;
    private int miercoles;
    private int jueves;
    private int viernes;
    private int sabados;
    private int domingos;
    private int cantidad_estudiantes;
    private int edad_minima;
    private int edad_maxima;
    private int con_edad;
    
    public Tandas(Integer id_tanda, Date anno_inicial, Date anno_final, 
            Date hora_inicial, Date hora_final, int lunes, int martes, 
            int miercoles, int jueves, int viernes, int sabados, int domingos, 
            int cantidad_estudiantes, int edad_minima, int edad_maxima, 
            int con_edad) {
        this.id_tanda = id_tanda;
        this.anno_inicial = anno_inicial;
        this.anno_final = anno_final;
        this.hora_inicial = hora_inicial;
        this.hora_final = hora_final;
        this.lunes = lunes;
        this.martes = martes;
        this.miercoles = miercoles;
        this.jueves = jueves;
        this.viernes = viernes;
        this.sabados = sabados;
        this.domingos = domingos;
        this.cantidad_estudiantes = cantidad_estudiantes;
        this.edad_minima = edad_minima;
        this.edad_maxima = edad_maxima;
        this.con_edad = con_edad;
    }    
    public Integer getId_tanda() {
        return id_tanda;
    }
    public void setId_tanda(Integer id_tanda) {
        this.id_tanda = id_tanda;
    }
    public Date getAnno_inicial() {
        return anno_inicial;
    }
    public void setAnno_inicial(Date anno_inicial) {
        this.anno_inicial = anno_inicial;
    }
    public Date getAnno_final() {
        return anno_final;
    }
    public void setAnno_final(Date anno_final) {
        this.anno_final = anno_final;
    }
    public Date getHora_inicial() {
        return hora_inicial;
    }
    public void setHora_inicial(Date hora_inicial) {
        this.hora_inicial = hora_inicial;
    }
    public Date getHora_final() {
        return hora_final;
    }
    public void setHora_final(Date hora_final) {
        this.hora_final = hora_final;
    }
    public int getLunes() {
        return lunes;
    }
    public void setLunes(int lunes) {
        this.lunes = lunes;
    }
    public int getMartes() {
        return martes;
    }
    public void setMartes(int martes) {
        this.martes = martes;
    }
    public int getMiercoles() {
        return miercoles;
    }
    public void setMiercoles(int miercoles) {
        this.miercoles = miercoles;
    }
    public int getJueves() {
        return jueves;
    }
    public void setJueves(int jueves) {
        this.jueves = jueves;
    }
    public int getViernes() {
        return viernes;
    }
    public void setViernes(int viernes) {
        this.viernes = viernes;
    }
    public int getSabados() {
        return sabados;
    }
    public void setSabados(int sabados) {
        this.sabados = sabados;
    }
    public int getDomingos() {
        return domingos;
    }
    public void setDomingos(int domingos) {
        this.domingos = domingos;
    }
    public int getCantidad_estudiantes() {
        return cantidad_estudiantes;
    }
    public void setCantidad_estudiantes(int cantidad_estudiantes) {
        this.cantidad_estudiantes = cantidad_estudiantes;
    }
    public int getEdad_minima() {
        return edad_minima;
    }
    public void setEdad_minima(int edad_minima) {
        this.edad_minima = edad_minima;
    }
    public int getEdad_maxima() {
        return edad_maxima;
    }
    public void setEdad_maxima(int edad_maxima) {
        this.edad_maxima = edad_maxima;
    }
    public int getCon_edad() {
        return con_edad;
    }
    public void setCon_edad(int con_edad) {
        this.con_edad = con_edad;
    }
}