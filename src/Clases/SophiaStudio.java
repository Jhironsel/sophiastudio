package Clases;

import Formulario.frmLogin;

public class SophiaStudio {
    public static void main(String[] args) {
        
        //Iniciamos la Base de Datos con esta instancia....
        fb_connection misFb = new fb_connection();
        
        //Instanciamos el El Formulario de Logeado... para Arrancar....
        frmLogin f = new frmLogin();
        
        //Le pasamos el Objecto de la coneccion
        f.setMisFb(misFb);
        
        //Hacemos el Formulario Visible y Centralizado...
        f.setVisible(true);
        f.setLocationRelativeTo(null);
    }

    public SophiaStudio() {
        //Iniciamos la Base de Datos con esta instancia....
        fb_connection misFb = new fb_connection();
        
        //Instanciamos el El Formulario de Logeado... para Arrancar....
        frmLogin f = new frmLogin();
        
        //Le pasamos el Objecto de la coneccion
        f.setMisFb(misFb);
        
        //Hacemos el Formulario Visible y Centralizado...
        f.setVisible(true);
        f.setLocationRelativeTo(null);
    }
    
}
