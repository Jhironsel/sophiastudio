package Clases;
public class Opcion {
    private String Valor;
    private String descripcion;
    

    public Opcion(String Valor, String descripcion) {
        this.Valor = Valor;
        this.descripcion = descripcion;
    }

    public String getValor() {
        return Valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
    
    
    
}
