package Clases;

public class Usuarios {
    private Integer idUsuario;//Identificacion del Usuarios Actual
    private String nombres;//Nombre del Usuarios del Sistema
    private String apellidos;//Apellidos del Usuarios del sistema
    private String clave;//Clave del Usuarios del sistema
    private Integer perfil;//Perfiles Programador, Administrador, Cajero
    private Integer estado;//Estado del Usuarios del sistema

    public Usuarios(String nombres, String apellidos, String clave, 
            Integer perfil, Integer estado) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.clave = clave;
        this.perfil = perfil;
        this.estado = estado;
    }

    public Usuarios(Integer idUsuario, String nombres, String apellidos, 
            String clave, Integer perfil, Integer estado) {
        this.idUsuario = idUsuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.clave = clave;
        this.perfil = perfil;
        this.estado = estado;
    }

    
    
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Integer getPerfil() {
        return perfil;
    }

    public void setPerfil(Integer perfil) {
        this.perfil = perfil;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
}