package Clases;
public class Encabezado {
    private String nombreInstitucion;//Nombre de la Institucion
    private String rnc;//Registro nacional contribullente
    private String direccion;//Direccion del Lugar
    private String lugar;//Ciudad o Pueblo
    private String Telefono;//Telefono 1
    private String Telefono2;//Telefono 2
    private String Telefono3;//Telefono 3

    public Encabezado(String nombreInstitucion, String rnc, String direccion, 
            String lugar, String Telefono, String Telefono2, String Telefono3) {
        this.nombreInstitucion = nombreInstitucion;
        this.rnc = rnc;
        this.direccion = direccion;
        this.lugar = lugar;
        this.Telefono = Telefono;
        this.Telefono2 = Telefono2;
        this.Telefono3 = Telefono3;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public String getRnc() {
        return rnc;
    }

    public void setRnc(String rnc) {
        this.rnc = rnc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String Telefono) {
        this.Telefono = Telefono;
    }

    public String getTelefono2() {
        return Telefono2;
    }

    public void setTelefono2(String Telefono2) {
        this.Telefono2 = Telefono2;
    }

    public String getTelefono3() {
        return Telefono3;
    }

    public void setTelefono3(String Telefono3) {
        this.Telefono3 = Telefono3;
    }    
}
