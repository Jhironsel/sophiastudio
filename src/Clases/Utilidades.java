package Clases;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilidades {
    public static String formatDate(Date fecha) {
        if (fecha == null) {
            fecha = new Date();
        }
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
        return formatoDelTexto.format(fecha);
    }
    
    public static String objectToString(Object Obj) {
        String Str = "";
        if (Obj != null) {
            Str = Obj.toString();
        }
        return Str;
    }
    public static Date objectToDate(Object obj) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        Date aux = null;
        try {
            aux = formatoDelTexto.parse(objectToString(obj));
        } catch (Exception ex) {
        }
        return aux;
    }
    public static Date objectToDate(String obj) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        Date aux = null;
        try {
            aux = formatoDelTexto.parse(objectToString(obj));
        } catch (Exception ex) {
        }
        return aux;
    }
    public static Date objectToTime(Object obj){
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("hh:mm:ss");
        Date aux = null;
        try {
            aux = formatoDelTexto.parse(objectToString(obj));
        } catch (Exception ex) {
        }
        return aux;
    }
}
