package Clases;
/**
 *
 * @author jhiro
 */
public class Perfiles {
    private Integer IdPerfil;
    private String perfil;
    private Integer frm_Padres;
    private Integer frm_Padres_Registro;
    private Integer frm_Padres_Modificar;
    private Integer frm_Padres_Borrar;
    private Integer frm_Estudiantes;
    private Integer frm_Estudiantes_Registro;
    private Integer frm_Estudiantes_Modificar;
    private Integer frm_Estudiantes_Borrar;
    private Integer frm_Usuarios;
    private Integer frm_Usuarios_Registro;
    private Integer frm_Usuarios_Modificar;
    private Integer frm_Usuarios_Borrar;
    private Integer frm_Pagos;
    private Integer frm_Abono;
    private Integer frm_Reportes;
    private Integer frm_Ajuste_Tanda;
    private Integer frm_Ajuste_Perfil_Usuario;

    public Perfiles(Integer IdPerfil, String perfil, Integer frm_Padres, 
            Integer frm_Padres_Registro, Integer frm_Padres_Modificar, 
            Integer frm_Padres_Borrar, Integer frm_Estudiantes, 
            Integer frm_Estudiantes_Registro, Integer frm_Estudiantes_Modificar, 
            Integer frm_Estudiantes_Borrar, Integer frm_Usuarios, 
            Integer frm_Usuarios_Registro, Integer frm_Usuarios_Modificar, 
            Integer frm_Usuarios_Borrar, Integer frm_Pagos, Integer frm_Abono, 
            Integer frm_Reportes, Integer frm_Ajuste_Tanda, 
            Integer frm_Ajuste_Perfil_Usuario) {
        this.IdPerfil = IdPerfil;
        this.perfil = perfil;
        this.frm_Padres = frm_Padres;
        this.frm_Padres_Registro = frm_Padres_Registro;
        this.frm_Padres_Modificar = frm_Padres_Modificar;
        this.frm_Padres_Borrar = frm_Padres_Borrar;
        this.frm_Estudiantes = frm_Estudiantes;
        this.frm_Estudiantes_Registro = frm_Estudiantes_Registro;
        this.frm_Estudiantes_Modificar = frm_Estudiantes_Modificar;
        this.frm_Estudiantes_Borrar = frm_Estudiantes_Borrar;
        this.frm_Usuarios = frm_Usuarios;
        this.frm_Usuarios_Registro = frm_Usuarios_Registro;
        this.frm_Usuarios_Modificar = frm_Usuarios_Modificar;
        this.frm_Usuarios_Borrar = frm_Usuarios_Borrar;
        this.frm_Pagos = frm_Pagos;
        this.frm_Abono = frm_Abono;
        this.frm_Reportes = frm_Reportes;
        this.frm_Ajuste_Tanda = frm_Ajuste_Tanda;
        this.frm_Ajuste_Perfil_Usuario = frm_Ajuste_Perfil_Usuario;
    }
    public Integer getIdPerfil() {
        return IdPerfil;
    }
    public void setIdPerfil(Integer IdPerfil) {
        this.IdPerfil = IdPerfil;
    }
    public String getPerfil() {
        return perfil;
    }
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    public Integer getFrm_Padres() {
        return frm_Padres;
    }
    public void setFrm_Padres(Integer frm_Padres) {
        this.frm_Padres = frm_Padres;
    }
    public Integer getFrm_Padres_Registro() {
        return frm_Padres_Registro;
    }
    public void setFrm_Padres_Registro(Integer frm_Padres_Registro) {
        this.frm_Padres_Registro = frm_Padres_Registro;
    }
    public Integer getFrm_Padres_Modificar() {
        return frm_Padres_Modificar;
    }
    public void setFrm_Padres_Modificar(Integer frm_Padres_Modificar) {
        this.frm_Padres_Modificar = frm_Padres_Modificar;
    }
    public Integer getFrm_Padres_Borrar() {
        return frm_Padres_Borrar;
    }
    public void setFrm_Padres_Borrar(Integer frm_Padres_Borrar) {
        this.frm_Padres_Borrar = frm_Padres_Borrar;
    }
    public Integer getFrm_Estudiantes() {
        return frm_Estudiantes;
    }
    public void setFrm_Estudiantes(Integer frm_Estudiantes) {
        this.frm_Estudiantes = frm_Estudiantes;
    }
    public Integer getFrm_Estudiantes_Registro() {
        return frm_Estudiantes_Registro;
    }
    public void setFrm_Estudiantes_Registro(Integer frm_Estudiantes_Registro) {
        this.frm_Estudiantes_Registro = frm_Estudiantes_Registro;
    }
    public Integer getFrm_Estudiantes_Modificar() {
        return frm_Estudiantes_Modificar;
    }
    public void setFrm_Estudiantes_Modificar(Integer frm_Estudiantes_Modificar) {
        this.frm_Estudiantes_Modificar = frm_Estudiantes_Modificar;
    }
    public Integer getFrm_Estudiantes_Borrar() {
        return frm_Estudiantes_Borrar;
    }
    public void setFrm_Estudiantes_Borrar(Integer frm_Estudiantes_Borrar) {
        this.frm_Estudiantes_Borrar = frm_Estudiantes_Borrar;
    }
    public Integer getFrm_Usuarios() {
        return frm_Usuarios;
    }
    public void setFrm_Usuarios(Integer frm_Usuarios) {
        this.frm_Usuarios = frm_Usuarios;
    }
    public Integer getFrm_Usuarios_Registro() {
        return frm_Usuarios_Registro;
    }
    public void setFrm_Usuarios_Registro(Integer frm_Usuarios_Registro) {
        this.frm_Usuarios_Registro = frm_Usuarios_Registro;
    }
    public Integer getFrm_Usuarios_Modificar() {
        return frm_Usuarios_Modificar;
    }
    public void setFrm_Usuarios_Modificar(Integer frm_Usuarios_Modificar) {
        this.frm_Usuarios_Modificar = frm_Usuarios_Modificar;
    }
    public Integer getFrm_Usuarios_Borrar() {
        return frm_Usuarios_Borrar;
    }
    public void setFrm_Usuarios_Borrar(Integer frm_Usuarios_Borrar) {
        this.frm_Usuarios_Borrar = frm_Usuarios_Borrar;
    }
    public Integer getFrm_Pagos() {
        return frm_Pagos;
    }
    public void setFrm_Pagos(Integer frm_Pagos) {
        this.frm_Pagos = frm_Pagos;
    }
    public Integer getFrm_Abono() {
        return frm_Abono;
    }
    public void setFrm_Abono(Integer frm_Abono) {
        this.frm_Abono = frm_Abono;
    }
    public Integer getFrm_Reportes() {
        return frm_Reportes;
    }
    public void setFrm_Reportes(Integer frm_Reportes) {
        this.frm_Reportes = frm_Reportes;
    }
    public Integer getFrm_Ajuste_Tanda() {
        return frm_Ajuste_Tanda;
    }
    public void setFrm_Ajuste_Tanda(Integer frm_Ajuste_Tanda) {
        this.frm_Ajuste_Tanda = frm_Ajuste_Tanda;
    }
    public Integer getFrm_Ajuste_Perfil_Usuario() {
        return frm_Ajuste_Perfil_Usuario;
    }
    public void setFrm_Ajuste_Perfil_Usuario(Integer frm_Ajuste_Perfil_Usuario) {
        this.frm_Ajuste_Perfil_Usuario = frm_Ajuste_Perfil_Usuario;
    }
}
