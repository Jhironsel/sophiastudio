package Clases;
public class Estudiantes {
    private String matricula;//Matricula del Estudiantes Codigo Primario
    private String padreMadreCedula;//Cedula del Padre
    private String nombres;//Nombre del Estudiantes
    private String apellidos;//Apellido del Estudiantes
    private Integer Tanda;//Tanda de Horario del Estudiantes
    private String fechaNacimiento;//Fecha Nacimiento del Estudiantes
    private String fechaIngreso;//Fecha de Ingreso
    private String telefono;//Telefono del los Padres     
    private String estado;//Situacion del Estudiantes Activo, Inactivo, Suspendido
    private Integer idUsuario;

    public Estudiantes(String matricula, String padreMadreCedula, String nombres, 
            String apellidos, Integer Tanda, String fechaNacimiento, 
            String fechaIngreso, String telefono, String estado, 
            Integer idUsuario) {
        this.matricula = matricula;
        this.padreMadreCedula = padreMadreCedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.Tanda = Tanda;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaIngreso = fechaIngreso;
        this.telefono = telefono;
        this.estado = estado;
        this.idUsuario = idUsuario;
    }
    
    
    public String getMatricula() {
        return matricula;
    }
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    public String getPadreMadreCedula() {
        return padreMadreCedula;
    }
    public void setPadreMadreCedula(String padreMadreCedula) {
        this.padreMadreCedula = padreMadreCedula;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public Integer getTanda() {
        return Tanda;
    }
    public void setTanda(Integer Tanda) {
        this.Tanda = Tanda;
    }
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    public String getFechaIngreso() {
        return fechaIngreso;
    }
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public Integer getIdUsuario() {
        return idUsuario;
    }
    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }    
}
