package sonido;

import javazoom.jl.player.Player;

public class Sonido {
    public void reproducirAudio(String archivo) {
        try {
            Player player = new Player(getClass().getResourceAsStream(archivo));
            player.play();
            player.close();
        } catch (Exception e) {
            System.out.println("Error al reproducir el audio");
        }
    }
}
